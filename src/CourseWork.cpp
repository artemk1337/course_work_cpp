#include "CourseWork.h"


map<string, struct FILTERS> filters_map {
                                         {"ELA", {2, error_level_analysis,
                                                  "\n2 params:"
                                                  "\n\tcompression quality jpeg, int, from 1 to 99"
                                                  "\n\tmultiplying values to increase 'brightness', int, >1"}},
                                         {"CLAHE", {1, clahe_algorithm,
                                                    "\n1 param:"
                                                    "\n\tnumber of filter applications, int, >0"}},
                                         {"LUM", {1, luminance_gradient,
                                                  "\n1 param:"
                                                  "\n\tfilter application intensity, int, from 0 to 90"}},
                                         {"HSV", {1, hsv_transform,
                                                  "\n1 param:"
                                                  "\n\t0-HSV, 1-H, 2-S, 3-V, int"}},
                                         {"BRIGH", {1, change_brightness,
                                                    "\n1 param:"
                                                    "\n\tvalue, double, >=0"}},
                                         {"GAMMA", {2, gamma_correction,
                                                    "\n2 params:"
                                                    "\n\tgamma value, double, >0"
                                                    "\n\tpower, int, >0"}},
                                         {"CONTR", {1, change_contrast,
                                                    "\n1 param:"
                                                    "\n\tcontrast level, double, >0"}},
                                         {"AUTOCONT", {0, autocontrast,
                                                       "\n0 params"}},
                                         {"FEDG", {0, find_edges,
                                                   "\n0 params"}},
                                         {"THRES", {0, threshold_alg,
                                                    "\n0 params"}},
                                         {"LoG", {1, gaussian_laplasian,
                                                  "\n1 param:"
                                                  "\n\tscale, int, >0"}},
                                         {"DCT", {1, dct_transform,
                                                  "\n1 param:"
                                                  "\n\tstep, int, >0"}},
                                         {"HISTEQ", {0, hist_eq,
                                                     "\n0 params"}}};


Mat apply_filter(const Mat& img_src, char** argv)
{
    Mat img_dst = img_src.clone();

    while (argv[0]) {
        float params[MAX_PARAMS];
        string filter_name = (string)argv[0];
        // cout << filter_name << endl;
        argv += 1;
        for (int i = 0; i < filters_map[filter_name].params; i++)
            params[i] = stof((string) argv[i]);
        argv += filters_map[filter_name].params;
        img_dst = filters_map[filter_name].func(img_dst, params);
    }

    return img_dst;
}


int process_image(const string& filename, char** argv)
{
    Mat bgr_image = imread(filename);
    if (bgr_image.empty()) {
        cout  << "Could not open the input image: " << filename << endl;
        return -1;
    }
    Mat img_dst = apply_filter(bgr_image, argv);
    hconcat(img_dst, bgr_image, img_dst);
    imwrite("RES.png", img_dst);

    if (img_dst.size().height > 1080)
        resize(img_dst, img_dst, Size(),
               1080. / img_dst.size().height,
               1080. / img_dst.size().height);
    if (img_dst.size().width > 1920)
        resize(img_dst, img_dst, Size(),
               1920. / img_dst.size().width,
               1920. / img_dst.size().width);
    imshow("Image", img_dst);
    waitKey();
    return 0;
}


int process_video(const string& filename, char** argv)
{
    Mat frame;
    VideoCapture cap(filename);
    if (!cap.isOpened()) {
        cout  << "Could not open the input video: " << filename << endl;
        return -1;
    }

    auto frame_count = static_cast<float>(cap.get(CAP_PROP_FRAME_COUNT));
    Size S = Size((int) cap.get(CAP_PROP_FRAME_WIDTH),
                  (int) cap.get(CAP_PROP_FRAME_HEIGHT));
    VideoWriter outputVideo("out.avi", VideoWriter::fourcc('X','V','I','D'),
                            cap.get(CAP_PROP_FPS), S, true);

    if (!outputVideo.isOpened()) {
        cout  << "Could not open the output video for writing. "<< endl;
        return -1;
    }


    auto time_start = std::chrono::system_clock::now();

    float i = 0.;
    for (;;) {

        cap >> frame;
        if (frame.empty()) {
            cout << "\nFINISH" << endl;
            break;
        }
        frame = apply_filter(frame, argv);
        outputVideo << frame;
        chrono::duration<double> elapsed_seconds = chrono::system_clock::now() - time_start;
        cout << "\r" << "Procent: " << (int)(i / frame_count * 100.) << "%\tSec left: " << elapsed_seconds.count() / i * (frame_count - i) << " sec";
        // cout << (float) time_delta / (float) frame_count * (float) (frame_count - i) << "%";
        i += 1;
    }
    return 0;
}


void print_help(const string& arg) {
    if (!(arg.compare("--help")) || !(arg.compare("-h"))) {
        for (auto &it : filters_map) {
            string key = it.first;
            cout << "Name: " << it.first << it.second.description << endl;
        }
    }
}


int main(int argc, char** argv) {
    if (argc < 2)
        return -1;
    string filename = (string)argv[1];
    if ((argv[1][0] == '-') || (argv[1][0] == '-' && argv[1][1] == '-'))
        print_help((string)argv[1]);
    else if (process_image(filename, argv+2))
        process_video(filename, argv+2);
    return 0;
}
