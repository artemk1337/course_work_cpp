#include "CourseWork.h"

string get_max_min_value(const Mat& src_img) {
    double min, max;
    minMaxLoc(src_img, &min, &max);
    return to_string(max) + " " + to_string(min);
}

Mat set_max_255_min_0(const Mat& src_img) {
    Mat res = src_img.clone();
    double min, max;
    minMaxLoc(src_img, &min, &max);
    res -= min;
    res *= (255 / max);
    return res;
}

string cvtype2str(int type) {
    string r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);

    switch ( depth ) {
        case CV_8U:  r = "8U"; break;
        case CV_8S:  r = "8S"; break;
        case CV_16U: r = "16U"; break;
        case CV_16S: r = "16S"; break;
        case CV_32S: r = "32S"; break;
        case CV_32F: r = "32F"; break;
        case CV_64F: r = "64F"; break;
        default:     r = "User"; break;
    }

    r += "C";
    r += to_string(chans + '0');

    return r;
}


Mat get_dct_block(Mat block)
{
    block.convertTo(block, CV_32F);
    dct(block, block);
    //cout << get_max_min_value(block) << endl;
    //cout << get_max_min_value(block) << endl;
    block = set_max_255_min_0(block);
    block.convertTo(block, CV_8U);
    //cout << get_max_min_value(block) << endl;
    return block;
}

Mat dct_transform(const Mat& img, const float params[MAX_PARAMS])
{
    Mat img_dst = img.clone();

    auto step = (int)params[0];
    assert(step > 0);

//    if (img_dst.size().width % step)
//        resize(img_dst, img_dst,
//               Size(img_dst.size().width - (img_dst.size().width % step), img_dst.size().height));
//    if (img_dst.size().height % step)
//        resize(img_dst, img_dst,
//               Size(img_dst.size().width, img_dst.size().height - (img_dst.size().height % step)));
    cout << img_dst.size() << endl;

    int ch = 3;
    vector<Mat> rgb_planes(ch);
    split(img_dst, rgb_planes);

    for (int c = 0; c < ch; c++) {
        cout << "Channel: " << c << endl;
        int j_steps = 0;
        int j = 0;
        while(j + step <= rgb_planes[c].size().height) {
            int i_steps = 0;
            int i = 0;
            while (i + step <= rgb_planes[c].size().width) {
                Mat im = rgb_planes[c];
                Rect area(i, j, step, step);
                Mat dst_roi = rgb_planes[c](Rect(i, j, step, step));
                get_dct_block(im(area)).copyTo(dst_roi);
                i += step;
                i_steps++;
            }
            j += step;
            j_steps++;
        }
    }
    merge(rgb_planes, img_dst);

    return img_dst;
}


Mat hist_eq(const Mat& img, const float params[MAX_PARAMS])
{
    Mat img_dst = img.clone();


    int ch = 3;
    vector<Mat> rgb_planes(ch);
    split(img_dst, rgb_planes);

    for (int c = 0; c < ch; c++) {
        equalizeHist(rgb_planes[c], rgb_planes[c]);
    }

    merge(rgb_planes, img_dst);

    return img_dst;
}


Mat gaussian_laplasian(const Mat& img, const float params[MAX_PARAMS])
{
    Mat img_dst, gray, abs_dst;


    auto scale = (int)params[0];
    int ch = 3;
    /// Remove noise by blurring with a Gaussian filter
    GaussianBlur(img, img_dst, Size(3,3), scale, scale, BORDER_DEFAULT);
    vector<Mat> rgb_planes(ch);
    split(img_dst, rgb_planes);
    for (int i = 0; i < ch; i++) {
        /// Apply Laplace function
        Laplacian(rgb_planes[i], img_dst, CV_16S, 3, scale, 0, BORDER_DEFAULT);
        convertScaleAbs(img_dst, abs_dst);
        abs_dst.convertTo(rgb_planes[i], CV_8U);
        // cvtColor(abs_dst, rgb_planes[i], COLOR_GRAY2BGR);
    }
    merge(rgb_planes, img_dst);
    return img_dst;
}


Mat threshold_alg(const Mat& img, const float params[MAX_PARAMS])
{
    Mat img_dst;

    int ch = 3;
    vector<Mat> rgb_planes(ch);
    split(img, rgb_planes);

    for (int i = 0; i < ch; i++) {
        adaptiveThreshold(rgb_planes[i], rgb_planes[i], 255,
                          ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 13, 5);
        rgb_planes[i].convertTo(rgb_planes[i], CV_8U);
    }
    merge(rgb_planes, img_dst);

    return img_dst;
}


Mat find_edges(const Mat& img, const float params[MAX_PARAMS])
{
    Mat img_dst;
    float kdata[] = {-1, -1, -1, -1, 8, -1, -1, -1, -1};
    Mat kernel(3,3,CV_32F, kdata);

    int ch = 3;
    vector<Mat> rgb_planes(ch);
    split(img, rgb_planes);

    for (int i = 0; i < ch; i++) {
        filter2D(rgb_planes[i], rgb_planes[i], CV_32F, kernel);
        rgb_planes[i].convertTo(rgb_planes[i], CV_8U);
    }
    merge(rgb_planes, img_dst);

    return img_dst;
}


Mat autocontrast(const Mat& img, const float params[MAX_PARAMS])
{
    Mat img_dst;
    double min, max;
    minMaxLoc(img, &min, &max);

    double alow = min;
    double ahigh = max;
    int amax = 255;
    int amin = 0;

    // #calculate alpha, beta
    double alpha = ((amax - amin) / (ahigh - alow));
    double beta = amin - alow * alpha;
    // #perform the operation g(x,y)= α * f(x,y)+ β
    convertScaleAbs(img, img_dst, alpha, beta);
    return img_dst;
}


Mat change_contrast(const Mat& img, const float params[MAX_PARAMS])
{
    Mat img_dst;
    auto contrast = params[0];
    assert(contrast >= 0);

    img.convertTo(img_dst, -1, contrast, 0);
    return img_dst;
}


Mat change_brightness(const Mat& img, const float params[MAX_PARAMS])
{
    Mat img_dst;
    auto brightness = (int)params[0];
    assert(brightness != 0);

    img.convertTo(img_dst, -1, 1, brightness);
    return img_dst;
}


Mat gamma_correction(const Mat& img, const float params[MAX_PARAMS]) {
    Mat new_img;
    double power_value;
    Vec3d color;

    float gamma = params[0];
    int pow_gamma = (int)params[1];

    power_value = 1 / pow(gamma, pow_gamma);
    img.convertTo(new_img, CV_64F);
    new_img /= 255.0;
    pow(new_img, power_value, new_img);
    new_img *= 255.0;
    new_img.convertTo(new_img, CV_8U);
    return new_img;
}

//Mat error_level_analysis(const Mat& src_img, const float params[MAX_PARAMS]) {
//    Mat compressed_img;
//    Mat diff_img;
//    double min_, max_;
//    vector<int> compression_params;
//
//    int jpg_quality = (int)params[0];
//    int factor = (int)params[1];
//
//    compression_params.push_back(IMWRITE_JPEG_QUALITY);
//    compression_params.push_back(jpg_quality);
//    imwrite("tmp.jpg", src_img, compression_params);
//
//    compressed_img = imread("tmp.jpg");
//    absdiff(src_img, compressed_img, diff_img);
//    minMaxLoc(diff_img, &min_, &max_);
//    diff_img -= min_;
//    diff_img *= (255.0 / (max_ - min_));
//    diff_img *= factor;
//    diff_img.convertTo(diff_img, CV_8UC3);
//
//    return diff_img;
//}


Mat error_level_analysis(const Mat& src_img, const float params[MAX_PARAMS]) {
    Mat compressed_img;
    Mat diff_img;
    vector<int> compression_params;

    int jpg_quality = (int)params[0];
    int factor = (int)params[1];

    int ch = 3;
    vector<Mat> rgb_planes(ch);
    split(src_img, rgb_planes);

    compression_params.push_back(IMWRITE_JPEG_QUALITY);
    compression_params.push_back(jpg_quality);

    for (int i = 0; i < ch; i++) {
        imwrite("tmp.jpg", rgb_planes[i], compression_params);
        compressed_img = imread("tmp.jpg");
        cvtColor(compressed_img, compressed_img, COLOR_BGR2GRAY);
        absdiff(rgb_planes[i], compressed_img, diff_img);
        diff_img = set_max_255_min_0(diff_img);
        diff_img *= factor;
        diff_img.convertTo(rgb_planes[i], CV_8U);
    }
    merge(rgb_planes, diff_img);

    return diff_img;
}



Mat clahe_algorithm(const Mat& src_img, const float params[MAX_PARAMS])
{
    Mat lab_img;
    cvtColor(src_img, lab_img, COLOR_BGR2Lab);

    // Extract the L channel
    vector<Mat> lab_planes(3);
    split(lab_img, lab_planes);

    // apply the CLAHE algorithm to the L channel
    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(4);

    int apply_times = (int) params[0];
    Mat dst;

    for (int i = 0; i < apply_times; i++)
        clahe->apply(lab_planes[0], lab_planes[0]);

    // Merge the the color planes back into an Lab image
    // dst.copyTo(lab_planes[0]);
    merge(lab_planes, lab_img);

    // convert back to RGB
    Mat img_dst;
    cvtColor(lab_img, img_dst, COLOR_Lab2BGR);

    return img_dst;
}


Mat luminance_gradient(const Mat& src_img, const float params[MAX_PARAMS])
{
    int ch = 3;
    Mat img_dst;
    src_img.convertTo(img_dst, CV_64F);

    vector<Mat> rgb_planes(ch);
    split(img_dst, rgb_planes);

    float fraction = params[0] / 100;
//    int ksize = (int)params[1];
//    int scale = (int)params[2];

    for (int i = 0; i < ch; i++) {
        Mat gradX, gradY, gradXY;

        Sobel(rgb_planes[i], gradX, CV_64F, 1, 0, 5);
        Sobel(rgb_planes[i], gradY, CV_64F, 0, 1, 5);

        pow(gradX, 2.0, gradX);
        pow(gradY, 2.0, gradY);
        pow(gradX + gradY, 0.5, gradXY);

        addWeighted(rgb_planes[i], fraction, gradXY, 1.0 - fraction, 0, rgb_planes[i]);
        rgb_planes[i].convertTo(rgb_planes[i], CV_8U);
    }

    merge(rgb_planes, img_dst);
//    img_dst.convertTo(img_dst, CV_8U);

    return img_dst;
}


Mat hsv_transform(const Mat& src_img, const float params[MAX_PARAMS])
{
    Mat img_dst;

    Mat HSV[3];
    cvtColor(src_img, img_dst, COLOR_BGR2HSV);
    split(img_dst, HSV);

    int param = int(params[0]);

    if (param == 0) {
        hconcat(HSV, 3, img_dst);
    }
    else {
        img_dst = HSV[param - 1];
    }
    cvtColor(img_dst, img_dst, COLOR_GRAY2BGR);
    img_dst.convertTo(img_dst, CV_8UC3);
    cout << typeToString(img_dst.type());

    return img_dst;
}


//Mat pca_algorithm(const Mat& src_img, const float params[MAX_PARAMS])
//{
//    Mat img_dst;
//    Mat gray_img;
//    Mat img1d;
//    src_img.convertTo(gray_img, COLOR_BGR2GRAY);
//    cout << gray_img.size << endl;
//    img1d = gray_img.reshape(1, gray_img.rows * gray_img.cols);
//    cout << img1d.data << endl;
//    cout << img1d.size << endl;
//    //PCA pca(img1d, img_dst, PCA::DATA_AS_ROW, 2);
//    //cout << pca.mean << endl;
//    cout << gray_img.size << endl;
//    //img_dst = pca.backProject(img1d);
//    //cout << img_dst << endl;
//
//
//
//    Mat image = gray_img;
//    Mat flat = image.reshape(1, image.total()*image.channels());
//    vector<uchar> vec = image.isContinuous()? flat : flat.clone();
//    PCA pca(vec, Mat(), PCA::DATA_AS_ROW, 2);
//    cout << vec.size() << endl;
//
//
//    return src_img;
//}
