﻿cmake_minimum_required (VERSION 3.8)

set( PRJ_NAME "CourseWork" )

project ( ${PRJ_NAME} )

add_subdirectory(src)

add_executable (${PRJ_NAME} "src/CourseWork.cpp" "src/filters.cpp" "src/CourseWork.h")

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )
target_link_libraries( ${PRJ_NAME} ${OpenCV_LIBS} )

# TODO: Добавьте тесты и целевые объекты, если это необходимо.

