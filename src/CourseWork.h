﻿// CourseWork.h : включаемый файл для стандартных системных включаемых файлов
// или включаемые файлы для конкретного проекта.

#include <iostream>
#include <opencv2/opencv.hpp>
#include <ctime>

#define MAX_PARAMS 25

using namespace cv;
using namespace std;



struct FILTERS {
    int params;
    function<Mat(Mat, float[])> func;
    string description;
};



string cvtype2str(int type);


// filters.cpp

Mat error_level_analysis(const Mat& src_img, const float params[MAX_PARAMS]);
Mat gamma_correction(const Mat& img, const float params[MAX_PARAMS]);
Mat clahe_algorithm(const Mat& src_img, const float params[MAX_PARAMS]);
Mat luminance_gradient(const Mat& src_img, const float params[MAX_PARAMS]);
Mat hsv_transform(const Mat& src_img, const float params[MAX_PARAMS]);
Mat change_brightness(const Mat& img, const float params[MAX_PARAMS]);
Mat change_contrast(const Mat& img, const float params[MAX_PARAMS]);
Mat autocontrast(const Mat& img, const float params[MAX_PARAMS]);
Mat find_edges(const Mat& img, const float params[MAX_PARAMS]);
Mat threshold_alg(const Mat& img, const float params[MAX_PARAMS]);
Mat gaussian_laplasian(const Mat& img, const float params[MAX_PARAMS]);
Mat dct_transform(const Mat& img, const float params[MAX_PARAMS]);
Mat hist_eq(const Mat& img, const float params[MAX_PARAMS]);
//Mat pca_algorithm(const Mat& src_img, const float params[MAX_PARAMS]);






// TODO: установите здесь ссылки на дополнительные заголовки, требующиеся для программы.




